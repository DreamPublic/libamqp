package pkg

import (
	"fmt"
	"log"
	"strconv"
	"testing"
	"time"
)

func GetChannel() *AmqpChannel {
	amqpConnection := GetAmqpConnection()
	return NewAmqpChannel(amqpConnection)
}

func TestNewAmqpChannel(t *testing.T) {
	amqpConnection := GetAmqpConnection()
	amqpChannel := NewAmqpChannel(amqpConnection)
	time.Sleep(time.Second * 10)
	amqpChannel.Disconnect()
	time.Sleep(time.Second)
	amqpConnection.Disconnect()
	time.Sleep(time.Second * 10)
}

func TestNewAmqpChannel2(t *testing.T) {
	amqpConnection := GetAmqpConnection()
	_ = NewAmqpChannel(amqpConnection)
	time.Sleep(time.Second * 10)
	amqpConnection.Disconnect()
	time.Sleep(time.Second * 10)
}

func TestAmqpChannel_AddConsumer(t *testing.T) {
	amqpConnection := GetAmqpConnection()
	channel := NewAmqpChannel(amqpConnection)
	consumerChan := channel.AddConsumer("testQueue", "abcd", 1)
	go func() {
		for {
			msg, ok := <-consumerChan
			if !ok {
				break
			}
			println(string(msg.Body))
			_ = msg.Ack(false)
		}
	}()
	time.Sleep(time.Second * 10)
	channel.RemoveConsumer("abcd")
	time.Sleep(time.Second * 10)
	channel.Disconnect()
	amqpConnection.Disconnect()
	time.Sleep(time.Second * 10)
}

func TestAmqpChannel_DeclareQueue(t *testing.T) {
	channel := GetChannel()

	qName, err := channel.DeclareQueue("DeclareQueueNameTT", true, true)
	if nil != err {
		fmt.Println(err)
	} else {
		fmt.Println(qName)
	}

	qName, err = channel.DeclareQueue("DeclareQueueNameTF", true, false)
	if nil != err {
		fmt.Println(err)
	} else {
		fmt.Println(qName)
	}

	qName, err = channel.DeclareQueue("DeclareQueueNameFT", false, true)
	if nil != err {
		fmt.Println(err)
	} else {
		fmt.Println(qName)
	}

	qName, err = channel.DeclareQueue("DeclareQueueNameFF", false, false)
	if nil != err {
		fmt.Println(err)
	} else {
		fmt.Println(qName)
	}
}

func TestAmqpChannel_DeclareExchange(t *testing.T) {
	channel := GetChannel()

	err := channel.DeclareExchange("DeclareExchangeD", "direct", false, false)
	if nil != err {
		log.Println(err)
		t.FailNow()
	}

	err = channel.DeclareExchange("DeclareExchangeF", "fanout", false, false)
	if nil != err {
		log.Println(err)
		t.FailNow()
	}

	err = channel.DeclareExchange("DeclareExchangeT", "topic", false, false)
	if nil != err {
		log.Println(err)
		t.FailNow()
	}

	err = channel.DeclareExchange("DeclareExchangeH", "headers", false, false)
	if nil != err {
		log.Println(err)
		t.FailNow()
	}

	err = channel.DeclareExchange("DeclareExchangeTT", "direct", true, true)
	if nil != err {
		log.Println(err)
		t.FailNow()
	}

	err = channel.DeclareExchange("DeclareExchangeFT", "direct", false, true)
	if nil != err {
		log.Println(err)
		t.FailNow()
	}

	err = channel.DeclareExchange("DeclareExchangeTF", "direct", true, false)
	if nil != err {
		log.Println(err)
		t.FailNow()
	}
}

func TestBindAndUnBind(t *testing.T) {
	channel := GetChannel()
	_ = channel.DeclareExchange("bindExchange", "direct", false, true)
	_, _ = channel.DeclareQueue("bindQueue", false, true)
	err := channel.Bind("bindQueue", "*", "bindExchange")

	if nil != err {
		log.Println(err)
		t.FailNow()
	}

	err = channel.Unbind("bindQueue", "*", "bindExchange")
	if nil != err {
		log.Println(err)
		t.FailNow()
	}
}

func TestSendExchange(t *testing.T) {
	channel := GetChannel()
	err := channel.SendToExchange("tb", "acc", "hello")
	log.Println(err)
	err = channel.SendToQueue("PUSH_SERVICE_QUEUE", "hello")
	log.Println(err)
	time.Sleep(time.Second * 1000)
}

func TestAmqpChannel_DeleteQueue(t *testing.T) {
	channel := GetChannel()

	qName, err := channel.DeclareQueue("DeclareQueueNameTT", true, true)
	if nil != err {
		fmt.Println(err)
	} else {
		fmt.Println(qName)
	}

	err = channel.DeleteQueue("DeclareQueueNameTT", false, false)
	if nil != err {
		t.FailNow()
	}
}

func TestAmqpChannel_DeleteExchange(t *testing.T) {
	channel := GetChannel()
	_ = channel.DeclareExchange("bindExchange", "direct", false, true)
	_, _ = channel.DeclareQueue("bindQueue", false, true)
	err := channel.Bind("bindQueue", "*", "bindExchange")

	if nil != err {
		log.Println(err)
		t.FailNow()
	}

	err = channel.DeleteExchange("bindExchange", false)
	if nil != err {
		log.Println(err)
		t.FailNow()
	}
}

func TestCrash(t *testing.T) {
	con := NewAmqpConnection(AmqpConnectOption{
		Host:     "127.0.0.1",
		Port:     5672,
		Vhost:    "/",
		Username: "guest",
		Password: "guest",
	}, "test")
	con.Disconnect()
	time.Sleep(time.Second)
	for i := 0; i < 100; i++ {
		if i == 50 {
			con.Disconnect()
		}
		channel := NewAmqpChannel(con)
		queueName := strconv.Itoa(i)
		channel.DeclareQueue(queueName, false, true)
		channel.DeleteQueue(queueName, false, false)
		consumer := channel.AddConsumer(queueName, queueName, 1)
		_, ok := <-consumer
		fmt.Println(ok)
		channel.RemoveConsumer(queueName)
		channel.Disconnect()
	}
	select {}
}
