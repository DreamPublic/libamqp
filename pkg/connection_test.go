package pkg

import (
	"testing"
	"time"
)

func GetAmqpOption() AmqpConnectOption {
	return AmqpConnectOption{
		Host:     "localhost",
		Port:     5672,
		Vhost:    "/",
		Username: "guest",
		Password: "guest",
	}
}

func GetAmqpConnection() *AmqpConnection {
	return NewAmqpConnection(GetAmqpOption(), "test")
}

func TestNewAmqpConnection(t *testing.T) {
	amqpConnection := NewAmqpConnection(AmqpConnectOption{
		Host:     "localhost",
		Port:     5672,
		Vhost:    "/",
		Username: "guest",
		Password: "guest",
	}, "test")
	amqpConnection.Disconnect()

	time.Sleep(time.Second)

	amqpConnection = NewAmqpConnection(AmqpConnectOption{
		Host:     "localhost",
		Port:     5672,
		Vhost:    "",
		Username: "guest",
		Password: "guest",
	}, "test")
	amqpConnection.Disconnect()

	time.Sleep(time.Second)

	amqpConnection = NewAmqpConnection(AmqpConnectOption{
		Host:     "localhost",
		Port:     5672,
		Vhost:    "test",
		Username: "guest",
		Password: "guest",
	}, "test")
	amqpConnection.Disconnect()

	time.Sleep(time.Second)
}
